package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class GreedyKnapsack {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        new GreedyKnapsack().run();
        long finishTime = System.currentTimeMillis();
        System.out.println(finishTime - startTime + " ms");
    }

    private void run() {
            Scanner input = null;
            input = new Scanner(System.in);
            int n  = input.nextInt();
            int W = input.nextInt();
            Item []items = new Item[n];
            for (int i = 0;i<n; i++){
                items[i] = new Item(input.nextInt(),input.nextInt());
            }
            for (int i = 0; i <n; i++){
                System.out.println(items[i]);
            }
            Arrays.sort(items);
            System.out.println();
            for (int i = 0; i <n; i++){
                System.out.println(items[i]);
            }
            double result = 0;
            for (Item item : items) {
                if (item.weight <= W){
                    result += item.cost;
                    W -= item.weight;
                }else {
                    result += (double) item.cost * W / item.weight;
                    break;
                }
            }
            System.out.println("\nresult: " + result);
    }

    class Item implements Comparable<Item>{
        int cost;
        int weight;

        public Item(int cost, int weight) {
            this.cost = cost;
            this.weight = weight;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Item{");
            sb.append("cost=").append(cost);
            sb.append(", weight=").append(weight);
            sb.append('}');
            return sb.toString();
        }

        @Override
        public int compareTo(Item item) {
            long r1 = (long) cost * item.weight;
            long r2 = (long) item.cost * weight;
   //         double r1 = (double) cost / weight;
   //         double r2 = (double)item.cost/ item.weight;
            return -Double.compare(r1,r2 );        }
    }
}
