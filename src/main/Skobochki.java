package main;

import java.util.Stack;

public class Skobochki {
    public static void main(String[] args) {
        Skobochki skobochki = new Skobochki();
        skobochki.runTheApp();
    }

    private void runTheApp() {
        String input = "6 \n Hello, world! \n Hello, world!) " +
                "\n Hello, world!:) \n (::) \n (::)) \n [::))";
        smiles(input);
    }

    private void smiles(String input) {
        // if ("(){}[]".matches("[\\[\\]\\{\\}\\(\\)]+"))

        Stack<Character> stack = new Stack<>();
        for (String line : input.split("\n")) {
            if (line.trim().matches("\\d+"))
                continue;
            line = line.trim().replace(":)", "")
                    .replace(":}", "")
                    .replace(":]", "");
   //         System.out.println(line);
            //можно заменить regex String.matches("...").
            if (!line.contains("}")&&!line.contains(")")&&!line.contains("]")&&!line.contains("{")&&!line.contains("(")&&!line.contains("[")){
                System.out.println("0");
            }else{
                for (int i = 0; i < line.length(); i++) {
                    char cur = line.charAt(i);
                    if (cur == '{' || cur == '(' || cur == '[') {
                        stack.push(cur);
                    }
                    if (cur == '}' || cur == ')' || cur == ']') {
                        if (stack.isEmpty()) {
                            System.out.println("1");
                            break;
                        } else {
                            char last = stack.peek();
                            if (cur == '}' && last == '{' || cur == ')' && last == '(' || cur == ']' && last == '[') {
                                stack.pop();
                            }else {
                                System.out.println("2");
                                break;
                            }
                        }
                    }
                    if (i == line.length()-1 && stack.isEmpty()){
                        System.out.println("0");
                    }
                }
                if (!stack.isEmpty()){
                    if (stack.peek()=='{'||stack.peek()=='('||stack.peek()==']'){
                        System.out.println("3");
                        stack.clear();
                    }
                }
            }
        }
    }
}
