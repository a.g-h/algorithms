package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class HuffmanCoding {
    public static void main(String[] args) throws FileNotFoundException {
        long startTime = System.currentTimeMillis();
        new HuffmanCoding().run();
        long finishTime = System.currentTimeMillis();
        System.out.println(finishTime - startTime + " ms");
    }

    private void run() throws FileNotFoundException {

        randomString();

        Scanner input = new Scanner(new File("/home/agaevskihanada/IdeaProjects/Algorithms/src/huffman.txt"));
        String s = input.next();
        System.out.println(s);
        Map<Character, Integer> count = new HashMap<>();
        for (int i = 0; i< s.length(); i++){
            char c = s.charAt(i);
            if (count.containsKey(c)){
                count.put(c,count.get(c)+1);
            }else {
                count.put(c,1);
            }
        }

        Map<Character, Node> characterNodeMap = new HashMap<>();

        PriorityQueue<Node> priorityQueue = new PriorityQueue<>();
        for (Map.Entry<Character, Integer> entry : count.entrySet()){
            LeafNode node = new LeafNode(entry.getKey(),entry.getValue());
            characterNodeMap.put(entry.getKey(),node);
            priorityQueue.add(node);
        }
        int sum = 0;
        while (priorityQueue.size()>1){
            Node first = priorityQueue.poll();
            Node second = priorityQueue.poll();
            InternalNode node = new InternalNode(first,second);
            sum += node.sum;
            priorityQueue.add(node);
        }
        System.out.println(count.size() + " " + sum);
        Node root = priorityQueue.poll();
        if (count.size()==1)
            root.buildCode("0");
        else root.buildCode("");

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i<s.length();i++){
            char c = s.charAt(i);
            builder.append(characterNodeMap.get(c).code);
        }
        System.out.println(builder.toString());

    }

    private void randomString() throws FileNotFoundException {
        PrintWriter printWriter= new PrintWriter("/home/agaevskihanada/IdeaProjects/Algorithms/src/huffman.txt");
        int n = 10000;
        Random random = new Random();
        for (int i = 0; i< n;i++){
            printWriter.append((char)('a' + random.nextInt(26)));
        }
        printWriter.close();
    }

    class Node implements Comparable<Node>{
        final int sum;
        String code;

        void buildCode(String code){
            this.code = code;
        }

        public Node(int sum) {
            this.sum = sum;
        }

        @Override
        public int compareTo(Node node) {
            return Integer.compare(sum,node.sum);
        }
    }

    class InternalNode extends Node{
        Node left;
        Node right;

        @Override
        void buildCode(String code) {
            super.buildCode(code);
            left.buildCode(code + "0");
            right.buildCode(code + "1");
        }

        public InternalNode(Node left, Node right) {
            super(left.sum+right.sum);
            this.left = left;
            this.right = right;
        }
    }

    class LeafNode extends Node{
        char symbol;

        @Override
        void buildCode(String code) {
            super.buildCode(code);
            System.out.println(symbol + " : " + code);
        }

        public LeafNode(char symbol, int count) {
            super(count);
            this.symbol = symbol;
        }
    }
}
