package main;

import java.io.*;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Inversion {

    private int[] temp;
    private int[] arr;

    public static void main(String[] args) throws IOException {
        long startTime = System.currentTimeMillis();
        new Inversion().run();
        long finishTime = System.currentTimeMillis();
        System.out.println(finishTime - startTime + " ms");
    }

    private void run() throws IOException {

        generateTest();

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(input.readLine());
  //      Scanner scanner = new Scanner(new File("/home/agaevskihanada/IdeaProjects/Algorithms/src/inversion.txt"));
    //    int n = scanner.nextInt();
        arr = new int[n];
        String [] tokens = input.readLine().split(" ");
        for (int i =0; i<n; i++){
//            arr[i] = scanner.nextInt();
            arr[i] = Integer.parseInt(tokens[i]);
        }
        temp = new int[n];
        mergeSort(0,n);
        System.out.println("\n" + count);

        System.out.println("\n" + Arrays.toString(arr));
    }

    long count = 0;

    private void mergeSort(int l,  int r) {
        if (r <= l + 1)
            return;
        //arr[l..r-1] -> arr[l..m -1] arr[m..r -1]
        int m = (l+r)>>1;
        mergeSort(l,m);
        mergeSort(m,r);
        merge(l,m,r);
    }

    private void merge(int l, int m, int r){
        //arr[l..m - 1] arr[m..r - 1] -> temp[l..r -1] -> arr[l..r -1]
        int i = l;
        int j = m;
        for (int k = l;  k < r;k++){
            if (j == r || (i < m && arr[i] <= arr[j])){
                temp[k] = arr[i];
                i++;
            }else {
                //a[i, i+1 ... arr.length-1]>b[j]
                count += m - i;
                temp[k] = arr[j];
                j++;
            }
        }
        System.arraycopy(temp,l,arr,l,r-l);
    }

    private void generateTest() throws FileNotFoundException {
        PrintWriter writer = new PrintWriter("/home/agaevskihanada/IdeaProjects/Algorithms/src/inversion.txt");
        int n = 100000;
        Random random = new Random();
        writer.println(n);
        for (int i = 0; i < n; i++) {
            writer.print(random.nextInt( ) + " ");
        }
        writer.close();
    }
}
