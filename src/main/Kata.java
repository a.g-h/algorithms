package main;

import java.util.*;

public class Kata {

    public static void main(String[] args) {
        //   System.out.println(HighAndLow("8 3 -5 42 -1 0 0 -9 4 7 4 -4"));
        //    System.out.println(even_or_odd(5));
        //    System.out.println(countBits(1234));
        /*System.out.println(isSquare(-1));
        System.out.println(isSquare(0));
        System.out.println(isSquare(3));
        System.out.println(isSquare(4));
        System.out.println(isSquare(25));
        System.out.println(isSquare(26));*/
        //  System.out.println(trigrams("the quick red"));
        //  Assert.assertEquals(trigrams("the quick red"),"the he_ e_q _qu qui uic ick ck_ k_r _re red" );
      //  System.out.println(reverseBits(43261596));
           }

    public static String HighAndLow(String numbers) {
        String[] numbersArray = numbers.split(" ");
        System.out.println(Arrays.stream(numbersArray).mapToInt(Integer::parseInt).max().getAsInt());
        String result = Arrays.stream(numbersArray).max(String::compareTo).get();
        result += " " + Arrays.stream(numbersArray).min(String::compareTo).get();
        return result;
    }

    public static String even_or_odd(int number) {
        return (number % 2 == 0 ? "Even" : "Odd");
    }

    public static long countBits(int n) {
        return Integer.toBinaryString(n).replace("0", "").length();
    }

    public static boolean feast(String beast, String dish) {
        return beast.startsWith(String.valueOf(dish.charAt(0))) && beast.endsWith(String.valueOf(dish.charAt(dish.length() - 1)));
    }

    public static boolean isSquare(int n) {
        return ((Math.sqrt((double) n) % 1) == 0);
    }

    public static String trigrams(String phrase) {
        char[] arr = phrase.toCharArray();
        StringBuilder builder = new StringBuilder();
        int fast = 0, slow = 0;
        while (slow < arr.length - 2) {

            while (fast <= slow + 2 && fast < arr.length) {
                if (arr[fast] == ' ') {
                    builder.append("_");
                } else {
                    builder.append(arr[fast]);
                }
                fast++;
            }
            builder.append(" ");
            slow++;
            fast = slow;
        }
        return builder.toString().trim();
    }

    public static int reverseBits(int n) {
        System.out.println("00000010100101000001111010011100");
        System.out.println(Integer.toBinaryString(n));
        StringBuilder builder = new StringBuilder(Integer.toBinaryString(n));
        return Integer.parseInt(builder.reverse().toString(), 2);
    }

    public int missingNumber(int[] nums) {
        /*Arrays.sort(nums);
        int i;
        for (i = 0; i < nums.length; i++) {
            if (nums[i] != i) {
                return i;
            }
        }
        return i;*/

        int expected = (1 + nums.length) * nums.length / 2;
        int actual = 0;
        for (int num : nums) {
            actual += num;
        }
        return expected - actual;
    }

    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char cur = s.charAt(i);
            if (cur == '{' || cur == '(' || cur == '[') {
                stack.push(cur);
            }
            if (cur == '}' || cur == ')' || cur == ']') {
                if (stack.isEmpty()) {
                    return false;
                } else {
                    char last = stack.peek();
                    if (cur == '}' && last == '{' || cur == ')' && last == '(' || cur == ']' && last == '[') {
                        stack.pop();
                    }
                }
            }
        }
        return stack.isEmpty();
    }



}
