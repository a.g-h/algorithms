package main;

import java.util.Arrays;

public class ShellSort {

    public static void main(String[] args) {
        ShellSort sort = new ShellSort();

        int [] nums = {3,6,1,7,2,8,10,4,9,5};
        System.out.println(Arrays.toString(nums));
        sort.shellSort(nums);
        System.out.println(Arrays.toString(nums));
    }

    private void shellSort(int[] nums) {
        int j;
        for (int gap = nums.length/2; gap > 0; gap /=2){
            for (int i =gap; i<nums.length;i++){
                int temp = nums[i];
                for (j = i; j>=gap && nums[j - gap] > temp; j-=gap){
                    nums[j] = nums[j - gap];
                }
                nums[j] = temp;
            }
        }
    }
}
