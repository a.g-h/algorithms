package main;

import sun.net.idn.StringPrep;

import java.util.Arrays;

public class InsertSort {
    public static void main(String[] args) {
        InsertSort sort = new InsertSort();
        int [] arr = {2,1,4,8,7,5,3,6};
        System.out.println(Arrays.toString(arr));
        sort.insertSort(arr);
        System.out.println(Arrays.toString(arr));
        System.out.println();

        String[] elements = { "Http","Tor", "Java","Tutorial","Dot", "Com"};
        System.out.println(Arrays.toString(elements));
        sort.insertionSort(elements);
        System.out.println(Arrays.toString(elements));
    }

    private void insertSort(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            int element = arr[i];
            int indexToInsert = i;

            while (indexToInsert>0 && arr[indexToInsert-1]> element){
                arr[indexToInsert] = arr[indexToInsert-1];
                indexToInsert--;
                arr[indexToInsert] = element;
            }
        }
    }

    private void insertionSort(String[] elements){
        for (int i = 0; i < elements.length; i++) {
            String element = elements[i];
            int indexToInsert = i;

            while (indexToInsert>0 && element.compareTo(elements[indexToInsert-1])<0){
                elements[indexToInsert] = elements[indexToInsert-1];
                indexToInsert--;
                elements[indexToInsert] = element;
            }
        }
    }
}
