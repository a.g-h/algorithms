package main;

import java.util.Arrays;

public class QuickSort {
    public static void main(String[] args) {
        QuickSort sort = new QuickSort();

        int[]array = {10,5,7,3,15};
        System.out.println(Arrays.toString(array));
        sort.quickSort(array);
        System.out.println(Arrays.toString(array));
    }

    private void quickSort(int[]arr) {
        int start = 0;
        int end = arr.length-1;
        doSort(arr,start,end );
    }

    private void doSort(int[] arr, int start, int end){
        if (start>=end)
            return;

        int pivot = start - (start-end)/2;
        while (start<end){
             while (start<pivot && (arr[start]<= arr[pivot])){
                 start++;
             }
             while (end > pivot && (arr[pivot]<= arr[end])){
                 end--;
             }
             if (start<end){
                 int temp = arr[start];
                 arr[start] = arr[end];
                 arr[end]= temp;
                 if (start == pivot)
                     pivot = end;
                 else if (end==pivot)
                     pivot = start;
             }
        }
        doSort(arr, start,pivot);
        doSort(arr, pivot+1,end);
    }
}
