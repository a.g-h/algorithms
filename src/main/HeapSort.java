package main;

import java.util.Arrays;

public class HeapSort {

    private int size;

    public static void main(String[] args) {
        HeapSort heapSort = new HeapSort();
        int [] numbers = {12,2,15,56,23,78,45,32,16,91,53,27};
        System.out.println(Arrays.toString(numbers));
        heapSort.heapSort(numbers);
        System.out.println(Arrays.toString(numbers));
    }

    private void heapSort(int[] numbers) {
        buildHeap(numbers);

        while (size >1){
            swap(numbers,0,--size);
            shiftDown(numbers, 0);
        }

        /*for (int i = numbers.length/2 -1; i >= 0 ; i--) {
            shiftDown(numbers, i, numbers.length);
        }
        for (int i = numbers.length-1; i > 0; i--) {
            swap(numbers, 0, i);
            shiftDown(numbers,0,i);
        }*/
    }

    private void buildHeap(int[] numbers) {
        size = numbers.length;
        for (int i = numbers.length/2 -1; i >= 0 ; i--) {
            shiftDown(numbers, i);
        }
    }

    private void shiftDown(int[] numbers, int i) {
        int left = leftChild(i);
        int right = rightChild(i);
        int max = i;

        if (left < size && numbers[i] < numbers[left]){
            max = left;
        }
        if (right < size && numbers[max] < numbers[right]){
            max = right;
        }
        if (i != max){
            swap(numbers,i, max);
            shiftDown(numbers,max);
        }
    }

    private void shiftDown(int [] numbers, int i, int n){
        int child;
        int temp;

        for (temp = numbers[i]; leftChild(i)<n; i = child){
            child=leftChild(i);
            if (child!=n-1 && (numbers[child] < numbers[child+1]))
                child++;
            if (temp < numbers[child])
                numbers[i]= numbers[child];
            else break;
        }
        numbers[i] = temp;
    }

    private int leftChild(int i){
        return 2*i+2;
    }

    private int rightChild(int i){
        return 2*i+1;
    }

    private void swap(int [] numbers, int i, int j){
        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }
}
