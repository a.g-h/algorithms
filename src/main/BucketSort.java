package main;

import java.util.Arrays;

public class BucketSort {
    public static void main(String[] args) {
        int [] nums = {3,6,1,7,2,8,10,4,9,5};
        System.out.println(Arrays.toString(nums));
        new BucketSort().bucketSort(nums);
        System.out.println(Arrays.toString(nums));
    }

    private void bucketSort(int[] nums) {
        int [] bucket = new int[nums.length+1];
        Arrays.fill(bucket,0 );

        for (int i = 0; i < nums.length; i++) {
            bucket[nums[i]]++;
        }

        int k = 0;

        for(int i = 0; i < bucket.length; i++){
            for (int j = 0; j< bucket[i]; j++){
                nums[k++] = i;
            }
        }
    }
}
