package main;

import java.util.Arrays;

public class MergeSort {
    public static void main(String[] args) {
        int [] nums = {3,6,1,7,2,8,10,4,9,5};

        System.out.print(Arrays.toString(nums));

        MergeSort sort = new MergeSort();
        sort.mergeSort(nums, 0, nums.length-1);

        System.out.println();
        System.out.print(Arrays.toString(nums));

    }

    private void mergeSort(int[] nums, int low, int high) {
            if (low < high){
                int mid = (low+high) /2;
                mergeSort(nums,low,mid);
                mergeSort(nums,mid+1,high);
                merge(nums,low,mid,high);
            }
    }

    private void merge(int[] nums, int low, int mid, int high) {
        int n = high - low +1;
        int[] temp = new int[n];

        int i = low;
        int j = mid +1;
        int k = 0;

        while (i<=mid || j<=high){
            if (i>mid)
                temp[k++] = nums[j++];
            else if (j > high)
                temp[k++] = nums[i++];
            else if (nums[i]<nums[j])
                temp[k++] = nums[i++];
            else
                temp[k++] = nums[j++];
        }

        for (j=0; j<n; j++){
            nums[low + j] = temp[j];
        }
    }

}
