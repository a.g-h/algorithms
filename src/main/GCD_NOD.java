package main;

public class GCD_NOD {
    public static void main(String[] args) {

        long startTime = System.currentTimeMillis();
        new GCD_NOD().run();
        long finishTime = System.currentTimeMillis();
        System.out.println(finishTime - startTime + " ms");
    }

    private void run(){
        System.out.println(gcd(114234242342L * 2231,114234242342L * 5412  ));
    }

    private long gcd(long a , long b){
        System.out.println(a + " " +b);
        if (a==0)
            return b;
        if (b==0)
            return a;
        if (a>=b){
            return gcd(a%b,b );
        }else {
            return gcd(a,b%a );
        }
        /*int result = 0;
        for (int i = 1; i <= Math.max(a, b); i++) {
            if (a%i == 0 && b % i == 0){
                result = i;
            }
        }
        return result;*/
    }
}
