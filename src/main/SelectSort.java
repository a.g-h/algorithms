package main;

import java.util.Arrays;

public class SelectSort {
    public static void main(String[] args) {
        SelectSort sort = new SelectSort();
        int[] unsortedArray = {2,1,4,8,7,5,6,3};
        System.out.println(Arrays.toString(unsortedArray));
        sort.selectSort(unsortedArray);
        System.out.println(Arrays.toString(unsortedArray));

        System.out.println();

        int[]numbers = {3,6,1,7,2,8,10,4,9,5};
        System.out.println(Arrays.toString(numbers));
        sort.selectionSort(numbers);
        System.out.println(Arrays.toString(numbers));
    }

    private void selectSort(int[] unsortedArray) {
        int minIndex ;
        for (int i = 0; i < unsortedArray.length-1; i++) {
            minIndex = i;
            for (int j = i; j < unsortedArray.length; j++) {
                if (unsortedArray[i]>unsortedArray[j]){
                    minIndex = j;
                }
            }
            int temp = unsortedArray[i];
            unsortedArray[i] = unsortedArray[minIndex];
            unsortedArray[minIndex]=temp;
        }
    }

    private void selectionSort(int[] numbers){
        for (int i = 0; i <= numbers.length-1; i++) {
            swap(numbers, i, getSmallest(numbers, i, numbers.length-1));
        }
    }

    private int getSmallest(int [] numbers, int low, int high){
        int small = low;
        for (int i = low+1; i <= high; i++) {
            if (numbers[i]<numbers[small])
                small = i;
        }
        return small;
    }

    private void swap(int[]numbers, int current, int smallest){
        int temp = numbers[current];
        numbers[current] = numbers[smallest];
        numbers[smallest] = temp;
    }
}
