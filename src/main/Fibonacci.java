package main;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {

    private static final int MOD = (int) (1e9+7);

  //  Map<Integer, BigInteger> cache = new HashMap<>();
  //  List<BigInteger> cache = new ArrayList<>();

    /*List<Integer> cache = new ArrayList<>();

    {
        *//*cache.add(BigInteger.ZERO);
        cache.add(BigInteger.ONE);*//*
        cache.add(0);
        cache.add(1);
    }*/

//    int count;

    public static void main(String[] args) {
        /*for (int i = 0; i < 100; i++) {
            long startTime = System.currentTimeMillis();
            new Fibonacci().run(i);
            long finishTime = System.currentTimeMillis();
            System.out.println(finishTime - startTime + " ms");
        }
*/
        long startTime = System.currentTimeMillis();
        new Fibonacci().run(100000);
        long finishTime = System.currentTimeMillis();
        System.out.println(finishTime - startTime + " ms");
    }

    private Integer fibonacci(int n){

        int a = 0;
        int b = 1;
        for (int i = 0; i < n; i++) {
            int c = (a+b) % MOD;
            a = b;
            b = c;
        }

        return a;

        /*for (int i = cache.size(); i <= n; i++) {
            BigInteger res = cache.get(i-1).add(cache.get(i-2));
            cache.add(res);
        }
        return cache.get(n);*/

        /*for (int i = cache.size(); i <=n; i++) {
            int res = (cache.get(i -1) + cache.get(i-2)) % MOD;
            cache.add(res);
        }
        return cache.get(n);*/

        /*count ++;
        if (n<2)
            return BigInteger.valueOf(n);

        if (cache.containsKey(n)){
            return cache.get(n);
        }else {
            for (int i = 2; i <= n; i++) {
                BigInteger res = fibonacci(i-1).add(fibonacci(i-2));
                cache.put(i,res );
            }
            return cache.get(n);
            *//*BigInteger res = fibonacci(n-1).add(fibonacci(n-2));
            cache.put(n,res);
            return res;*//*
        }*/
    }

    private void run(int n){
        System.out.println(n + ": " + fibonacci(n));
    //    System.out.println(count + " calls");
    }


}
