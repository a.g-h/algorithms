package main;

public class Recursion {
    public static void main(String[] args) {
        Recursion recursion = new Recursion();

        System.out.println("recursionFromNtoOne:" + recursion.recursionFromNtoOne(10) + "\n");

        System.out.println("recursionFromAtoB: " + recursion.recursionFromAtoB(20,15 ));
        System.out.println("recursionFromAtoB: " + recursion.recursionFromAtoB(10,15 )+ "\n");

        System.out.println("recursionAckermann: " +recursion.recursionAckermann(3,2 )+ "\n");

        double n = 64;
        if (recursion.recursionIsPowerOfTwo(n)==1){
            System.out.println("recursionIsPowerOfTwo: Yes");
        }else {
            System.out.println("recursionIsPowerOfTwo: No");
        }

        System.out.println("\nrecursionSumOfDigits:" + recursion.recursionSumOfDigits(123));

        System.out.println("\nrecursionDigitsReversePrint: ");
        System.out.println(recursion.recursionDigitsReversePrint(123) + "\n");

        System.out.println("recursionDigitsPrint: "+ recursion.recursionDigitsPrint(153));

        System.out.println("\nrecursionIsPrimeNumber: " + recursion.recursionIsPrimeNumber(18,2));

        System.out.println("\nrecursionIntegerFactorization:");
    }

    private String recursionFromNtoOne(int n) {
        // Базовый случай
        if (n == 1) {
            return "1";
        }
        // Шаг рекурсии / рекурсивное условие
        return recursionFromNtoOne(n - 1) + " " + n;
    }

    private String recursionFromAtoB(int a, int b) {
        if (a > b) {
            if (a == b)
                return Integer.toString(a);
            return (a + " " + recursionFromAtoB(a - 1, b));
        } else {
            if (a == b)
                return Integer.toString(a);
            return a + " " + recursionFromAtoB(a + 1, b);
        }
    }

    private int recursionAckermann(int m, int n){
        if (m==0)
            return n+1;
        else if (n==0 && m>0)
            return recursionAckermann(m-1,1 );
        else
            return recursionAckermann(m-1,recursionAckermann(m,n-1));
    }

    private int recursionIsPowerOfTwo(double n){
        if (n==1){
            return 1;
        }else if (n>1 && n<2)
            return 0;
        else
            return recursionIsPowerOfTwo(n/2);
    }

    private int recursionSumOfDigits(int n){
        if (n<10)
            return n;
        else
            return n%10 + recursionSumOfDigits(n/10);
    }

    private int recursionDigitsReversePrint(int n){
        if (n < 10)
            return n;
        else{
            System.out.print(n%10 + " ");
            return recursionDigitsReversePrint(n/10);
        }
    }

    private String recursionDigitsPrint(int n){
        if (n < 10)
            return Integer.toString(n);
        else
            return recursionDigitsPrint(n/10) + " " + n%10;
    }

    private boolean recursionIsPrimeNumber(int n, int i){
        if (n < 2)
            return false;
        else if (n==2)
            return true;
        else if (n % i ==0)
            return false;
        else if (i < n /2)
            return recursionIsPrimeNumber(n, i+1);
        else
            return true;
    }


}
